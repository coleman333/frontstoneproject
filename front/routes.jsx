import React from 'react';
import {Route, IndexRoute, IndexRedirect, Router} from 'react-router';
import App from './containers/App';
import MainPage from './containers/MainPage';
import Register from './containers/Register';
import Log from './containers/Log';
import Navigation from './containers/Navigation';
import Profile from './containers/Profile';
import Partners from './containers/Partners';
import Partner from './containers/Partner';
import PartnersUsers from './containers/PartnersUsers';
import Verification from "./containers/Verification";
import Users from "./containers/Users";

export default (store) => {
    function requireAuth(nextState, replace, callback) {
        const authUser = store.getState().getIn(['auth', 'user']);
        if (authUser.isEmpty()) {
            replace({
                pathname: '/login',
                // state:{nextPathname:nextState.location.pathname}
            });
        }
        callback();
    }

    function redirectUser(nextState, replace, callback) {
        const authUser = store.getState().getIn(['auth', 'user']);
        if (!authUser.isEmpty()) {
            replace({
                pathname: '/'
            });
        }
        callback();
    }

    // onEnter={requireAuth}



    return (
        <Route>
            <Route path="/" component={App}>
                <IndexRedirect to="profile"/>
                <Route component={Navigation}>
                    <Route path="profile" component={Profile}/>
                    <Route path="users" component={Users}/>
                    <Route path="partners">
                        <IndexRoute component={Partners}/>
                        <Route path=":id" component={Partner}>

                            <IndexRedirect to="users"/>

                            <Route path="users-partners" component={PartnersUsers}/>
                            <Route path="products" component="div"/>
                            <Route path="bills" component="div"/>
                        </Route>
                    </Route>
                </Route>

                <Route onEnter={redirectUser}>
                    <Route path="register" component={Register}/>
                    <Route path="login" component={Log}/>
                    <Route path="verification" component={Verification}/>
                </Route>
            </Route>
        </Route>

    )
}

