import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

// createActionAsync = require('redux-act-async');

module.exports = {
    registration: createActionAsync('REGISTRATION', (user2)=> {
        console.log(user2);
        let user=new FormData();
        user.append('avatar',user2.files);
        user.append('firstName',user2.firstName);
        user.append('lastName',user2.lastName);
        user.append('email',user2.email);
        user.append('password',user2.password);
        return axios({
            method: 'post',
            url: '/api/users/registration',
            data: user
        });
        // .then(
        //     {
        //        
        //     }
        // )
    }, {rethrow: true}),

    login: createActionAsync('LOGIN', (userData)=> {
        return axios({
            method: 'post',
            url: '/api/users/login',
            data: userData
        })
    }, {rethrow: true}),

    logout:createActionAsync('LOGOUT',()=>{
        return axios({
            method:'get',
            url:'/api/users/logout'

        })
        // console.log(33,action))
    }, {rethrow: true}),

    ISEmail:createActionAsync('ISEMAIL',(email)=>{
        // console.log(33,action);
        // console.log(email);
        return axios({
            method:'post',
            url:'/api/users/isEmail',
            data:{email:email}
        })
    
    }, {rethrow: true})

};
