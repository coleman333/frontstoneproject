import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

module.exports = {
	loadCards : createActionAsync('LOADCARDS',( idUser,cardLimit ,skipCards)=>{
		return axios ({
			method:'post',
			url:'/api/cards/loadCards',
			data:{
				userId:idUser,
				cardLimit:cardLimit,
				skipCards:skipCards
			}
		})
	},{rethrow:true}),

	loadCardsMainPage:createActionAsync('LOADCARDSMAINPAGE',(cardLimit,skipCards)=>{
		// console.log(12345);
		return axios({
			method:'post',
			url:'/api/cards/loadCardsMP',
			data:{
				cardLimit:cardLimit,
				skipCards:skipCards
			}
		})
	},{rethrow:true}),

	redirectToUserCard:createActionAsync('REDIRECTTOUSERCARD',(idUser,idCard)=>{
		return axios({
			method:'post',
			url:'/api/cards/redirectToUserCard',
			data:{
				idUser:idUser,
				idCard:idCard
			}
		})
	})
};
//
// module.exports={
// 	loadCardsMainPage:createActionAsync('LOADCARDSMAINPAGE',(cardLimit,skipCards)=>{
// 		console.log(12345);
// 		return axios({
// 			method:'post',
// 			url:'api/cards/loadCardsMP',
// 			data:{
// 				cardLimit:cardLimit,
// 				skipCards:skipCards
// 			}
// 		})
// 	},{rethrow:true})
//
// };