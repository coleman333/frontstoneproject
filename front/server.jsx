import React from 'react';
import Immutable from 'immutable';
import {renderToString} from 'react-dom/server';
import {RouterContext, match} from 'react-router';
import {Provider} from 'react-redux';
import createRoutes from './routes';
import configureStore from './store/configureStore';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();
import _ from 'lodash';
import axios from 'axios';

const clientConfig = {
	host: process.env.HOSTNAME || 'localhost',
	port: process.env.PORT || '80'
};

// configure baseURL for axios requests (for serverside API calls)
axios.defaults.baseURL = `http://${clientConfig.host}:${clientConfig.port}`;

/*
 * Our html template file
 * @param {String} renderedContent
 * @param initial state of the store, so that the client can be hydrated with the same state as the server
 * @param head - optional arguments to be placed into the head
 */
function renderFullPage(renderedContent, initialState, head = {
	title: '<title>React template</title>',
	meta: '<meta name="viewport" content="width=device-width, initial-scale=1" />' +
	'<meta charset="utf-8">',
	link: '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">' +
	'<link rel="stylesheet" href="/assets/styles/main.css"/>'
}) {
	return `
  <!doctype html>
    <html lang="en">
    

    <head>
        ${head.title}

        ${head.meta}

        ${head.link}
         <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/x-icon">-->
           <link rel="shortcut icon" href="/images/favicon.png" type="image/png">


    </head>
    <body>
    <div id="app" style="height: 100%"  class="container-fluid">${renderedContent}</div>

    <script>
      window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
    </script>
    <script type="text/javascript" charset="utf-8" src="/assets/front.js"></script>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </body>
    </html>`;
}

/*
 * Export render function to be used in server/routes/index.js
 * We grab the state passed in from the server and the req object from Express/Koa
 * and pass it into the Router.run function.
 */
module.exports = (req, res) => {

	let initialState = Immutable.fromJS({
		auth: {
			processing: 0,
			error: {},
			user: {},
		}
	});

	if (req.isAuthenticated()) {

		const userObject = req.user.toObject();
		const user = {
			firstName: userObject.firstName,
			lastName: userObject.lastName,
			email: userObject.email,
			avatar: userObject.avatar
		};
		user._id = userObject._id.toString();
		initialState = initialState.setIn(['auth', 'user'], Immutable.fromJS(user));
	}

	const store = configureStore(initialState);

	const routes = createRoutes(store);

	/*
	 * This function is to be used for server-side rendering. It matches a set of routes to
	 * a location, without rendering, and calls a callback(error, redirectLocation, renderProps)
	 * when it's done.
	 */
	match({routes, location: req.url}, (error, redirectLocation, renderProps) => {
		if (error) {
			res.status(500).send(error.message);
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search);
		} else if (renderProps) {
			const InitialView = (
				<Provider store={store}>
					<RouterContext {...renderProps} />
				</Provider>
			);

			const componentHTML = renderToString(InitialView);
			const initialState = store.getState().toJS();
			res.status(200).end(renderFullPage(componentHTML, initialState));
		} else {
			res.status(404).send('Not Found');
		}
	});
};
