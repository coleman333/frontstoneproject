import React, {Component} from "react";
import {connect} from "react-redux";

import {browserHistory} from 'react-router';
import enter from '../../images/key_enter.svg';
import support from '../../images/support.svg';
import TextField from 'material-ui/TextField';
import confirmIcon from '../../images/confirmation.svg';

class Log extends Component {
    static propTypes = {};

    state = {
        dropPasswordDialog: {
            open: false,
        }
    };

    constructor(props) {
        super(props);
    }

    toggleDropPasswordDialog(value) {
        if (this.state.dropPasswordDialog.open === value) {
            return;
        }

        this.state.dropPasswordDialog.open = value;
        this.forceUpdate();
    }

    submitVerification(){
        browserHistory.push('/profile')
    }


    render() {
        const animateClass = _.get(this.state, 'dropPasswordDialog.open') ? 'animate' : 'disanimate';

        return (
            <div style={{display: 'flex', flex: 1}}>
                <div style={{flex: 8}}>
                    <div style={{display: 'flex', padding: '12% 19%', height: '100%',}}>
                        <div style={{
                            display: 'flex',
                            height: '100%',
                            flex: 1,
                        }}
                        >
                            <div style={{
                                backgroundColor: '#ffffff',
                                height: '100%',
                                flex: 1,
                                zIndex: 2,
                                borderRadius: 20,
                                boxShadow: '0.4em 0.4em 5px rgba(122,122,122,0.1)'
                            }}>
                                <div style={{height: '22%', width: '100%', display: 'flex'}}>
                                    <img src={confirmIcon} alt="" style={{
                                        width: '15%',
                                        height: '25%',
                                        marginLeft: '5%',
                                        marginTop: '10%'
                                    }}/>
                                    <div style={{
                                        marginLeft: '2%',
                                        marginTop: '8%',
                                        width: '30%',
                                    }}>
                                        <h2 style={{fontWeight: 550}}>Подтверждение личности</h2>
                                    </div>
                                </div>
                                <hr/>
                                <div style={{marginLeft: '12%', marginRight: '12%', marginTop: '20%'}}>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="usermail@company.com"
                                        fullWidth="true"
                                    />
                                    <br/>
                                    <TextField
                                        hintText="password"
                                        floatingLabelText="Введите пароль"
                                        type="password"
                                        fullWidth="true"
                                    /><br/>
                                </div>
                                <div style={{marginLeft: '12%', marginTop: '5%'}}>
                                    <button className="btn btnPrimary" style={{borderRadius: '20px', width: '30%'}}
                                            onClick={this.toggleDropPasswordDialog.bind(this, !_.get(this.state, 'dropPasswordDialog.open'))}>
                                        Войти
                                    </button>
                                </div>
                                <div style={{marginTop: '30%', marginLeft: '12%',color: '#8c96a0'}}>
                                    <div>
                                        <span>Код будет автоматически отправлен через {}</span>
                                    </div>
                                    <div>
                                        <a href="#"
                                           onClick={this.toggleDropPasswordDialog.bind(this, !_.get(this.state, 'dropPasswordDialog.open'))}>
                                            отправить код заново</a>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div style={{display: 'flex', height: '100%', flex: 1}}
                        >
                            <div className={animateClass} id="stripe" style={{
                                backgroundColor: '#ebeef4', height: '100%', flex: 1,
                                position: 'relative', zIndex: 1, borderRadius: 20,
                                boxShadow: '0.4em 0.4em 5px rgba(122,122,122,0.1)'
                            }}>
                                <div style={{height: '22%', width: '100%', display: 'flex'}}>
                                    <div style={{
                                        marginLeft: '20%',
                                        marginTop: '8%',
                                        width: '30%',
                                        font: 'bold'
                                    }}>
                                        <h2 style={{fontWeight: 550}}>Введите код</h2>
                                    </div>
                                </div>
                                <hr/>
                                <div style={{marginLeft: '20%', marginRight: '12%', marginTop: '20%'}}>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="Введите код от CMS"
                                        fullWidth="true"
                                    /><br/>

                                </div>
                                <div style={{marginLeft: '20%', marginTop: '5%'}}>
                                    <button className="btn btnPrimary" style={{borderRadius: '20px'}}
                                            onClick={this.submitVerification.bind(this)}>
                                      Подтвердить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*<div style={{marginLeft: '30%', marginTop: '2%'}}>*/}
                    {/*<span style={{fontSize: '12', color: '#4e87e5'}}>Если у вас еще нет аккаунта</span>*/}
                    {/*<a href="#">Зарегистрируйтесь</a>*/}
                    {/*</div>*/}

                </div>
                <div style={{flex: 2}}>
                    <div style={{marginLeft: '-10%', marginTop: '60%'}}>
                        <h5 style={{color: 'white'}}> простой мир</h5>
                        <h1 style={{color: 'white'}}>Управление</h1>
                    </div>

                    <div style={{display: 'flex', marginLeft: -70,marginTop: '105%'}}>
                        <img src={support} alt="" style={{height: 50}}/>
                        <div style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'flex-end',
                            marginLeft: 10
                        }}>
                            <div style={{fontSize: '12', color: '#86b9ff'}}>Техническая поддержка</div>
                            <div>
                                <a style={{color:'white'}} href="#">Guardians@simpleword.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Log);
