import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import "../assets/styles/main.scss";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {browserHistory} from 'react-router';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router';
import Registration from './Registration';
import Login from './Login';

import authActions from '../actions/auth';
import IconButton from 'material-ui/IconButton';

import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';


import TextField from 'material-ui/TextField';
import search from '../../images/search.svg';


class MainPage extends Component {
	static propTypes = {};

	state = {
		open: false,
		openRegistration: false,
		// messages: [],
		openLogin: false,
		// slideIndex: 1,
		selectedBottomIndex: 0
	};

	constructor(props) {
		super(props);

		// this.state.slideIndex=0;
		// this.state = Object.assign({}, this.state, {slideIndex: 0});

	}

	handleOpen = () => {
		this.setState({open: true});
	};

	onRegistrationToggle(value) {
		this.setState({
			openRegistration: value
		})
	}

	handleToggle(ev) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		// this.state.open=true;
		// this.forceUpdate()
		this.setState({
			open: !this.state.open
		})
	}


	onLoginToggle(value) {
		this.setState({
			openLogin: value
		})
	}

	closeDrawerToggle() {
		this.setState({
			open: false
		})
	}

	logoutToggle() {
		this.props.authActions.logout()
			.then((res) => {
				// this.handleClose();
				browserHistory.push('/')
				if (this.props.onSubmitSuccess) {
					this.props.onSubmitSuccess(ev);
				}
				//close the panel
				// console.log('ok', res)
				this.closeDrawerToggle();

			})
			.catch(err => {
				console.log(err)

			})
	}

	select = (index) => this.setState({selectedBottomIndex: index});

	// col row justify-content-end
	// style={{display:'flex', justifyContent:'flex-end'}}
	// <label htmlFor="" key="1">личный кабинет</label>,
// <FontIcon className="muidocs-icon-custom-github" />


	render() {

		const {
			children, user
		} = this.props;

		let content;
		if (!user.isEmpty()) {
			content = <MenuItem onTouchTap={this.logoutToggle.bind(this)}>Logout</MenuItem>;
			console.log(11, 'event')
		} else {
			content = [
				<MenuItem key="1" onTouchTap={this.onLoginToggle.bind(this, true)}>Login</MenuItem>,
				<MenuItem
					key="2"
					onTouchTap={this.onRegistrationToggle.bind(this, true)}>Registration</MenuItem>
			]
		}

		return (
			<MuiThemeProvider>
				<div className="container-fluid">
					<div className="row justify-content-end">

						<div className="" style={{display: 'flex', justifyContent: 'flex-end'}}>
							{
								!this.props.user.isEmpty() &&
								[

									<RaisedButton key="3" className="customIconBtn" label={user.get('firstName')}

																primary={true}
																icon={<img src={user.get('avatar')}/>}
																style={{margin: '10px'}}
									/>
								]
							}
						</div>

					</div>
					<div className="" style={{display: 'flex', justifyContent: 'center'}}>
						<IconButton className=" "
												tooltip="REGISTER OF LOGIN"
												tooltipPosition="bottom-left"
												onTouchTap={this.handleToggle.bind(this)}>
						</IconButton>
					</div>


					{children}

					<Drawer open={this.state.open} onRequestChange={() => console.log(123)}>
						{content}
						<MenuItem onTouchTap={this.closeDrawerToggle.bind(this)}>Cancel</MenuItem>
					</Drawer>

					<Registration
						onSubmitSuccess={this.handleToggle.bind(this)}
						onRegistrationToggle={this.onRegistrationToggle.bind(this)}
						open={this.state.openRegistration}
					/>
					<Login
						onSubmitSuccess={this.handleToggle.bind(this)}
						onLoginToggle={this.onLoginToggle.bind(this)}
						open={this.state.openLogin}
					/>

				</div>
			</MuiThemeProvider>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		user: state.getIn(['auth', 'user'])
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		authActions: bindActionCreators(authActions, dispatch)
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);

