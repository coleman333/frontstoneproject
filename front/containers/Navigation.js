import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import profileIcon from '../../images/icon_profile.svg';
import partnersIcon from '../../images/icon_partners.svg';
import usersIcon from '../../images/icon_users.svg';
import dmpIcon from '../../images/icon_DMP.svg';
import menuCollapseIcon from '../../images/burger_menu_collapse.svg';
import menuExpandIcon from '../../images/burger_menu_expand.svg';
import {browserHistory} from 'react-router';

class Navigation extends Component {
	static propTypes = {};

	state = {
		toggleMenu: false,
        menuIcon:menuCollapseIcon,

	};

	constructor(props) {
		super(props);
	}

	onToggleMenu(value) {
		// if (this.state.toggleMenu === value) {
		//     console.log(12341251,value);
		// 	return;
		// }

        console.log('this is menuIcon',this.state.menuIcon);
        if (this.state.toggleMenu === true) {
            this.state({menuIcon:menuExpandIcon})
            console.log('this is menuIcon',this.state.menuIcon);
        }
        else{
            this.state({menuIcon:menuCollapseIcon})
        }
        console.log(12341251,value);

		this.setState({toggleMenu: value});
	}

    redirectOnProfile(){
        browserHistory.push('/profile')
    }

    redirectOnPartners(){
        browserHistory.push('/partners')
    }

    redirectOnUsers(){
	    browserHistory.push('/users')
    }

	render() {
		const {children} = this.props;

		const minimized = this.state.toggleMenu;

		return (
			<div className="row" style={{height: '100%'}}>
				<div className="left-bar-menu col-lg-2"
						 style={{
							 backgroundColor: '#3e6fe4', maxWidth: minimized ? 50 : undefined,
							 display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
						 }}
				>
					<div>
						<img src={this.state.menuIcon} alt="" style={{height: 35, maxWidth: 50}}
								 onClick={this.onToggleMenu.bind(this, !minimized)}/>
						<div style={{padding: '40px 0'}}>
							<div style={{
								marginLeft: '20%', color: '#fff66c', fontSize: '24px',
								fontWeight: '600',
								display: minimized ? 'none' : 'block',
								lineHeight: 1,
								position: 'relative',
								width: 120
							}}>
								<div>Простой</div>
								<div>Mир</div>
								<img src={usersIcon} alt=""
										 style={{
											 height: 40,
											 position: 'absolute',
											 right: -20,
											 top: -25
										 }}/>
							</div>
						</div>
						<div className="menu-item row active" style={{minHeight: 56, display: 'flex'}}
						onTouchTap={this.redirectOnProfile.bind(this)}>
							<div className="col-xs-2 menu-item-icon" style={{flex: 2, padding: 10, maxWidth: 50}}>
								<img src={profileIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{flex: 10, paddingLeft: 10, alignItems: 'center', display: minimized ? 'none' : 'flex'}}>
								Профиль
							</div>
						</div>
						<div className="menu-item row" style={{minHeight: 56, display: 'flex'}}
                             onTouchTap={this.redirectOnPartners.bind(this)}>
							<div className="col-xs-2 menu-item-icon" style={{flex: 2, padding: 10, maxWidth: 50}}>
								<img src={partnersIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{flex: 10, paddingLeft: 10, alignItems: 'center', display: minimized ? 'none' : 'flex'}}>
								Партнеры
							</div>
						</div>
						<div className="menu-item row" style={{minHeight: 56, display: 'flex'}}
                             onTouchTap={this.redirectOnUsers.bind(this)}>
							<div className="col-xs-2 menu-item-icon" style={{flex: 2, padding: 10, maxWidth: 50}}>
								<img src={usersIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{flex: 10, paddingLeft: 10, alignItems: 'center', display: minimized ? 'none' : 'flex'}}>
								Пользователи
							</div>
						</div>
						<div className="menu-item row" style={{minHeight: 56, display: 'flex'}}>
							<div className="col-xs-2 menu-item-icon" style={{flex: 2, padding: 10, maxWidth: 50}}>
								<img src={dmpIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{flex: 10, paddingLeft: 10, alignItems: 'center', display: minimized ? 'none' : 'flex'}}>
								DMP данные
							</div>
						</div>
					</div>

					<div style={{marginBottom: 20, display: 'flex', justifyContent: 'center'}}>
						<button className="btn btn-primary"
                            style={{
                                borderRadius: '20px',
                                width: '60%',
                                justifyContent: 'center',
                                display: minimized ? 'none' : 'flex'
                            }}
						>
							Выход
						</button>
					</div>

					<div className="menu-item row" style={{
						marginBottom: 20, minHeight: 56,
						display: minimized ? 'flex' : 'none'
					}}>
						<div className="col-xs-2 menu-item-icon" style={{flex: 2, padding: 10, maxWidth: 50}}>
							<img src={dmpIcon} alt=""/>
						</div>
						<div className={`col-xs-10`}
								 style={{flex: 10, paddingLeft: 10, alignItems: 'center', display: minimized ? 'none' : 'flex'}}>
							Выход
						</div>
					</div>


				</div>
				<div className="col-lg-10">
					{children}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Navigation);
