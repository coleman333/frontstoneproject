import React, {Component} from "react";
import {connect} from "react-redux";
// import FlatButton from 'material-ui/FlatButton';
// import FontIcon from 'material-ui/FontIcon';
// import SelectField from 'material-ui/SelectField';
// import RaisedButton from 'material-ui/RaisedButton';
// import MenuItem from 'material-ui/MenuItem';
import {browserHistory} from 'react-router';
import search from '../../images/search.svg';
import TextField from 'material-ui/TextField';

class Register extends Component {
	static propTypes = {};

	state = {};

	constructor(props) {
		super(props);
	}


	render() {

		return (
			<div style={{display: 'box', display: 'flex'}}>
				<div className="" style={{height: '100%', width: '50%',}}>
					<div style={{
						backgroundColor: '#eaedf2',
						width: '42.3958%',
						height: '59.2593vh',
						left: '14.2188%',
						top: '22.4074%',
						marginLeft: '20%',
						marginTop: '20%',
						borderRadius: '20px'
					}}>
						<div style={{height: '22%', width: '100%', display: 'flex'}}>
							<img src={search} alt="" style={{
								width: '15%',
								height: '45%',
								marginLeft: '5%',
								marginTop: '8%'
							}}/>
							<div style={{
								marginLeft: '2%',
								marginTop: '5%',
								width: '30%'
							}}>
								<h3>Вход в систему</h3>
							</div>
						</div>
						<hr/>
						<div style={{marginLeft: '12%', marginRight: '12%', marginTop: '20%'}}>
							<TextField
								hintText=""
								floatingLabelText="Введите E-mail"
								fullWidth="true"
							/><br/>
							<TextField
								hintText="password"
								floatingLabelText="Введите пароль"
								fullWidth="true"
							/><br/>
						</div>
						<div style={{marginLeft: '12%', marginTop: '5%'}}>
							<button className="btn btnPrimary" style={{borderRadius: '20px', width: '30%'}}>Далее</button>
						</div>
						<div style={{marginTop: '30%', marginLeft: '65%'}}>
							<a href="#">забыли пароль?</a>
						</div>
					</div>
					<div style={{marginLeft: '30%', marginTop: '2%'}}>
						<span style={{fontSize: '12', color: '#8bb1ef'}}>Если у вас еще нет аккаунта</span><a
						href="#">Зарегистрируйтесь</a>
					</div>
				</div>

				<div style={{height: '100%', width: '49%'}}>
					<div style={{marginLeft: '40%', marginTop: '20%'}}>
						<h5 style={{color: 'white'}}> простой мир</h5>
						<h1 style={{color: 'white'}}>Управление</h1>
					</div>

					<div style={{marginLeft: '40%', marginTop: '40%'}}>
						<div>
							<img src={search} alt="" style={{width: '5%'}}/>
						</div>
						<div>
							<span style={{fontSize: '12', color: '#8bb1ef'}}>Техническая поддержка</span><br/>
							<a href="#">Guardians@simpleword.com</a>
						</div>
					</div>
				</div>
			</div>

		)
	}

}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Register);
