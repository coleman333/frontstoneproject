import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import addPhotoIcon from '../../images/icon_add_photo.svg';
import deleteUserIcon from '../../images/icon_delete.svg';
import passwdEditIcon from '../../images/icon_password_edit.svg';
import supportIcon from '../../images/support.svg';
import settingsIcon from '../../images/icon_more.svg';
import markerList from '../../images/icon_add_photo.svg';
import arrowDownIcon from '../../images/arrow down.svg';
import plusIcon from '../../images/icon_plus.svg';

import grayViewListIcon from '../../images/view-list.svg';
import blueViewTableIcon from '../../images/view-table.svg';
import blueViewListIcon from '../../images/view-list-blue.svg';
import grayViewTableIcon from '../../images/view-table-gray.svg';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import buttonDelete from '../../images/button_delete.svg';
import buttonEdit from '../../images/button_edit.svg';
import photoBox from '../../images/photo_box.svg';
import officeAvatar from '../../images/officeAvatar.jpg';
import partnersIcon from '../../images/icon_partners.svg';
import partnersEditIcon from '../../images/icon_parnter_edit.svg';

import {Tabs, Tab} from 'material-ui/Tabs';

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {fromJS} from "immutable";

class Partners extends Component {
    static propTypes = {};

    state = {
        showCheckboxes: false,
        height: '600px',
        selectable: false,
        slideIndex: 0,
        toggleToEditOrDeleteFlagId: -1,
        partners : [
            {
                name: 'Михаил Кравцов',
                city: 'city1',
                scores: '1245',
                activity: '25 апреля 2018',
            },
            {
                name: 'Зинаида полюшина',
                city: 'city2',
                scores: '2450',
                activity: '25 апреля 2018',
            },
            {
                name: 'Регина Астахова',
                city: 'city3',
                scores: '789',
                activity: '25 апреля 2018',
            },
            {
                name: 'Андрей зарин',
                city: 'city4',
                scores: '10266',
                activity: '25 апреля 2018',
            },
        ],
        userForModal:{},
        selectValueRoleUser: 2,
        userRoleFromSelect:'',

    };

    constructor(props) {
        super(props);
        // this.setState(this.state);
    }

    handleChange = (event, index, selectValueRoleUser) => {
        this.setState({selectValueRoleUser});
        // if(this.state.selectValueRoleUser==1){
        // //     this.state.userRoleFromSelect==="admin";
        // this.setState({userRoleFromSelect:'admin'});
        // }
        // if(this.state.selectValueRoleUser==2){
        // //     this.state.userRoleFromSelect==="user";
        //     this.setState({userRoleFromSelect:'user'});
        // }

            // this.setState({userRoleFromSelect});
        // console.log('this is role for user',this.state.selectValueRoleUser,this.state.userRoleFromSelect);
    };  //select role from modal addUser

    onChangeCitySelect(ev) {
        let city = ev.target.value;
        this.setState({selectedCity:city});
    }

    openAddPartnerWindow() {
        $('#addUserModal').modal({show: true});
    }

    openEditPartnerWindow(index) {
        $('#editUserModal').modal({show: true});
        // console.log('asdfhlasd;fk',index);
        // console.log('this is the edit partner', this.state.partners[index].name);
        this.setState({userForModal:this.state.partners[index]})
    }

    openDeletePartnerWindow(index) {
        $('#deleteUserModal').modal({show: true});
        this.setState({userForModal:this.state.partners[index]})
    }

    openDeleteUserProfileWindow() {
        $('#deleteUserModal').modal({show: true});
        $('#editUserModal').modal('hide')
    }

    toggleToEditOrDelete(index, ev) {
        if (ev) {
            ev.preventDefault()
            ev.stopPropagation()
        }
        if (this.state.toggleToEditOrDeleteFlagId === index) {
            index = -1;
        }
        //console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
        this.setState({toggleToEditOrDeleteFlagId: index});

    }

    submitAddNewPartner(){
        const newParner = {
            name: this.refs.name.input.value,
            contact:this.refs.city.input.value,
            city:this.refs.partner.input.value,
            phone:this.refs.phone.input.value,
            email:this.refs.email.input.value

        };
        $('#addUserModal').modal('hide');

        console.log(123434563456,newParner);
    }

    uploadFile() {

    }

    render() {
        // const {
        // 	partners = [
        // 		{
        // 			logo: addPhotoIcon,
        // 			name: 'companyName1',
        // 			city: 'city1',
        // 			contact: 'name1',
        // 		},
        // 		{
        // 			logo: deleteUserIcon,
        // 			name: 'companyName2',
        // 			city: 'city2',
        // 			contact: 'name2',
        // 		},
        // 		{
        // 			logo: passwdEditIcon,
        // 			name: 'companyName3',
        // 			city: 'city3',
        // 			contact: 'name3',
        // 		},
        // 		{
        // 			logo: passwdEditIcon,
        // 			name: 'companyName4',
        // 			city: 'city4',
        // 			contact: 'name4',
        // 		},
        // 		{
        // 			logo: passwdEditIcon,
        // 			name: 'companyName5',
        // 			city: 'city5',
        // 			contact: 'name5',
        // 		},
        // 		{
        // 			logo: passwdEditIcon,
        // 			name: 'companyName6',
        // 			city: 'city6',
        // 			contact: 'name6',
        // 		},
        // 	]
        // } = this.props;
        // this.setState({partners2:partners});

        // const {
        //     user = {
        //         firstName: 'Дмитрий',
        //         secondName: 'Заруба',
        //         role: 'superAdmin',
        //         phone: '+7 902 563 50 45',
        //         email: 'dzaruba@gmail.com',
        //         avatar: '../../images/officeAvatar.jpg'
        //     }
        // } = this.props;

        let partners = this.state.partners;

        if(this.state.selectedCity) {
            partners = partners.filter(p => p.city === this.state.selectedCity)
        }

        // const {officeAvatar: avatar} = user.avatar;
        return (
            <div className={'row'} style={{height: '100%'}}>
                <div className={'col-lg-12'}>
                    <div className="row">
                        <div className="col-lg-12" style={{backgroundColor: '#f8f8f8',}}>
                            {/*<div style={{display:'flex',flexDirection:'row',justifyContent: 'space-between'}}>*/}
                            {/*<div className="column">*/}

                            {/*<div className="col-lg-2">*/}
                            {/*<span style={{marginLeft: '5%', color: '#5a5a5a',fontSize:'14px'}}>Простой Мир Управление</span>*/}
                            {/*</div>*/}
                            {/*<div className="col-lg-2">*/}
                            {/*<span style={{marginLeft: '5%', color: '#6caaff', fontSize: '30px', fontWeight: 600}}>Профиль</span>*/}
                            {/*</div>*/}
                            {/*</div>*/}
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="col-lg-2">
                                        <span style={{color: '#5a5a5a', fontSize: '14px'}}>Простой Мир Управление</span>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-lg-2">
                                            <span style={{
                                                marginLeft: '5%', color: '#6caaff',
                                                fontSize: '30px', fontWeight: 600
                                            }}>Профиль</span>
                                        </div>
                                        <div className=" offset-lg-3"
                                             style={{display: 'flex', alignItems: 'center', color: '#5a5a5a',}}>Город:
                                        </div>
                                        <select className="form-control" id="exampleFormControlSelect1"
                                                style={{width: '20%', borderRadius: '20px', marginLeft: '2%'}}
                                                onChange={this.onChangeCitySelect.bind(this)} >
                                            <option value="">Все города</option>
                                            {this.state.partners.map((item, index) => {
                                                return <option value={item.city} key={index}>{item.city}</option>
                                            })}
                                        </select>

                                        <button className="btn btn-primary mb-2"
                                                onTouchTap={this.openAddPartnerWindow.bind(this)}
                                                style={{borderRadius: '20px', marginLeft: '2%'}}>
                                            <img src={plusIcon} style={{width: '25px'}} alt=""/>
                                            Добавить партнеров
                                        </button>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div className="row">
                        <div className="col-lg-12" style={{backgroundColor: "white", height: '100%'}}>
                            <div>
                                <Table height={this.state.height} selectable={this.state.selectable}>
                                    <TableHeader displaySelectAll={this.state.showCheckboxes}
                                                 adjustForCheckbox={this.state.showCheckboxes}
                                    >

                                        <TableRow>
                                            <TableHeaderColumn>№</TableHeaderColumn>
                                            <TableHeaderColumn>Имя Фамилия</TableHeaderColumn>
                                            <TableHeaderColumn>Город</TableHeaderColumn>
                                            <TableHeaderColumn>Баллы</TableHeaderColumn>
                                            <TableHeaderColumn>Активность</TableHeaderColumn>
                                            <TableHeaderColumn></TableHeaderColumn>
                                        </TableRow>
                                    </TableHeader>
                                    <TableBody displayRowCheckbox={this.state.showCheckboxes}
                                    >
                                        {partners.map((item, index) => {
                                            return <TableRow key={index}>
                                                <TableRowColumn style={{height: '10vh'}}>{index + 1}</TableRowColumn>
                                                <TableRowColumn >{item.name}</TableRowColumn>
                                                <TableRowColumn>{item.city}</TableRowColumn>
                                                <TableRowColumn>{item.scores}</TableRowColumn>

                                                <TableRowColumn>
                                                    <img src={markerList} alt=""
                                                         style={{width: '5px', marginRight: '3%'}}/>
                                                    {item.activity}
                                                </TableRowColumn>
                                                {this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
                                                    <img src={settingsIcon} alt=""
                                                         onClick={this.toggleToEditOrDelete.bind(this, index)}
                                                         style={{width: '25px', marginLeft: '90%'}}
                                                    /></TableRowColumn>}

                                                {this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
                                                    <div style={{width: '100%'}}>
                                                        <img src={buttonEdit} alt=""
                                                             onClick={this.openEditPartnerWindow.bind(this, index)}
                                                             style={{marginLeft: '40%'}}
                                                        />
                                                        <img src={buttonDelete} alt=""
                                                             onClick={this.openDeletePartnerWindow.bind(this, index)}
                                                        />
                                                    </div>
                                                </TableRowColumn>}
                                            </TableRow>
                                        })}


                                    </TableBody>
                                </Table>


                            </div>
                            <div className='row' style={{marginBottom: 20}}>
                                <div style={{display: 'flex', marginLeft: 100}}>
                                    <img src={supportIcon} alt="" style={{height: 50}}/>
                                    <div style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        justifyContent: 'flex-end',
                                        marginLeft: 10
                                    }}>
                                        <div style={{fontSize: '12', color: '#a6b1c0'}}>
                                            Техническая поддержка
                                        </div>
                                        <div>
                                            <a href="#">Guardians@simpleword.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

                <div className="modal fade" id="editUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="editUserModalLabel" aria-hidden="true"
                     style={{width: '450px', marginLeft: '40%', marginTop: '8%'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{borderRadius: '20px'}}>
                            <div className="modal-header">
                                <img src={partnersEditIcon} alt="" style={{width: '45px', marginTop: '10px'}}/>
                                <h3 className="modal-title" id="editUserModalLabel"
                                    style={{marginLeft: '5px', fontWeight: 600}}>
                                    Редактировать <br/>партнера</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <img className="imageClass" src={this.state.userForModal.logo} alt="" style={{
                                        width: '70px',
                                        height: '70px',
                                        borderRadius: '50%',
                                        backgroundSize: 'auto 70px',
                                        marginLeft: '7%',
                                        marginTop: '2%'
                                    }}/>
                                    {/*<img src={addPhotoIcon} alt=""*/}
                                    {/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%',cursor:'pointer'}}*/}
                                    {/*onTouchTap={this.changeAvatar.bind(this)}/>*/}
                                    {/*<FloatingActionButton mini={true} secondary={true}*/}
                                    {/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%'}}>*/}
                                    {/*/!*<img src={addPhotoIcon} style={{width:'25px'}} alt=""/>*!/*/}
                                    {/*<ContentAdd />*/}
                                    {/*<input type="file" onChange={this.uploadFile.bind(this)}/>*/}

                                    {/*</FloatingActionButton>*/}
                                    <label htmlFor="label123" style={{
                                        marginLeft: '-4%',
                                        width: '6%',
                                        marginTop: '10%',
                                        cursor: 'pointer'
                                    }}>
                                        <img src={addPhotoIcon} style={{width: '35px'}} alt=""/>
                                        <input type="file" id="label123" style={{display: 'none'}}
                                               onChange={this.uploadFile.bind(this)}/>
                                    </label>

                                    <img src={deleteUserIcon} alt=""
                                         style={{width: '20px', marginLeft: '15%', cursor: 'pointer'}}/>
                                    <span style={{color: '#e6144a', cursor: 'pointer'}}
                                          onTouchTap={this.openDeleteUserProfileWindow.bind(this)}>удалить партнера</span>
                                </div>
                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <TextField
                                        hintText={this.state.userForModal.name}
                                        floatingLabelText="название компании"
                                        fullWidth="true"
                                        ref="name"
                                    /><br/>

                                    <TextField
                                        hintText={this.state.userForModal.contact}
                                        floatingLabelText="Контактное лицо"
                                        fullWidth="true"
                                        ref="contact"
                                    /><br/>
                                    <TextField
                                        hintText={this.state.userForModal.city}
                                        floatingLabelText="Город"
                                        fullWidth="true"
                                        ref="city"
                                    /><br/>
                                    <button type="button"
                                            style={{borderRadius: '20px', width: '30%', marginLeft: '70%', marginTop: '10%'}}

                                            className="btn btn-primary">
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                            {/*<div className="modal-footer">*/}
                            {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                            {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="addUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="addUserModalLabel" aria-hidden="true"
                     style={{width: '450px', marginLeft: '40%', marginTop: '8%'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{borderRadius: '20px'}}>
                            <div className="modal-header">
                                <img src={partnersIcon} alt="" style={{width: '10%', marginTop: '10px'}}/>
                                <h3 className="modal-title" id="addUserModalLabel"
                                    style={{marginLeft: '5px', fontWeight: 600}}>
                                    Добавить<br/>пользователя</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {/*<div style={{display:'flex',flexDirection:'row'}}>*/}
                                    {/*<img className="imageClass" src={photoBox} alt="" style={{*/}
                                        {/*width: '70px',*/}
                                        {/*height: '70px',*/}
                                        {/*borderRadius: '50%',*/}
                                        {/*backgroundSize: 'auto 70px',*/}
                                        {/*marginLeft: '7%',*/}
                                        {/*marginTop: '2%'*/}
                                    {/*}}/>*/}

                                    {/*<label htmlFor="label123" style={{*/}
                                        {/*marginLeft: '-4%',*/}
                                        {/*width: '6%',*/}
                                        {/*marginTop: '10%',*/}
                                        {/*cursor: 'pointer'*/}
                                    {/*}}>*/}

                                        {/*<img src={markerList} style={{width: '35px',marginTop:'-305%'}} alt=""/>*/}
                                        {/*<input type="file" id="label123" style={{display: 'none'}}*/}
                                               {/*onChange={this.uploadFile.bind(this)}/>*/}
                                    {/*</label>*/}
                                    {/*<div style={{marginTop:'4%'}}>*/}
                                        {/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'80%'}}>*/}
                                            {/*Логотип</span><br/>*/}
                                        {/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'60%'}}>*/}
                                            {/*В формате PNG или JPG, не более 5Мб </span>*/}
                                    {/*</div>*/}

                                {/*</div>*/}
                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="фамилия имя отчество"
                                        fullWidth="true"
                                        ref="companyName"
                                    /><br/>
                                    <SelectField
                                        floatingLabelText="Город"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        fullWidth="true"
                                    >
                                        <MenuItem value={1} primaryText="admin" />
                                        <MenuItem value={2} primaryText="user" />
                                    </SelectField>
                                    <SelectField
                                            floatingLabelText="Партнер"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        fullWidth="true"
                                    >
                                        <MenuItem value={1} primaryText="admin" />
                                        <MenuItem value={2} primaryText="user" />
                                    </SelectField>
                                    <SelectField
                                        floatingLabelText="Роль"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        fullWidth="true"
                                    >
                                        <MenuItem value={1} primaryText="admin" />
                                        <MenuItem value={2} primaryText="user" />
                                    </SelectField>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="Телефон"
                                        // floatingLabelText="Введите номер мобильго телефона"
                                        fullWidth="true"
                                        ref="phone"
                                    /><br/>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="E-mail"
                                        fullWidth="true"
                                        ref="email"
                                    /><br/>

                                    <button type="button"
                                            style={{borderRadius: '20px', width: '30%', marginLeft: '70%', marginTop: '10%'}}
                                            onTouchTap={this.submitAddNewPartner.bind(this)}
                                            className="btn btn-primary">
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                            {/*<div className="modal-footer">*/}
                            {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                            {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="deleteUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="deleteUserModalLabel" aria-hidden="true"
                     style={{width: '450px', marginLeft: '40%', marginTop: '8%'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{borderRadius: '20px'}}>
                            <div className="modal-header">
                                <img src={deleteUserIcon} alt="" style={{width: '75px', marginTop: '10px'}}/>
                                <h3 className="modal-title" id="deleteUserModalLabel"
                                    style={{marginLeft: '5px', fontWeight: 600}}>
                                    Удаление<br/>пользователя</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <br/><br/>
                                    <label htmlFor="" style={{fontSize: '0.7vw'}}>Вы действительно хотите удалить
                                        парнера?</label>
                                    <br/><br/><br/><br/>
                                    <div>
                                        <h3>{this.state.userForModal.name} </h3><br/>
                                        <h3>{this.state.userForModal.contact} </h3><br/>
                                        {/*<span>{user.role}</span>*/}
                                    </div>
                                    <br/><br/><br/><br/><br/><br/><br/><br/>
                                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'flex-end'}}>
                                        <div className="" style={{width: '35%'}}>
                                            <button type="button"
                                                    style={{
                                                        borderRadius: '20px', width: '100%',
                                                        marginTop: '10%', marginBottom: '2%'
                                                    }}
                                                    className="btn btn">
                                                Отмена
                                            </button>
                                        </div>
                                        <div className="" style={{width: '35%'}}>
                                            <button type="button"
                                                    style={{
                                                        borderRadius: '20px', marginLeft: '8%', marginTop: '10%'
                                                        , width: '100%', marginBottom: '2%'
                                                    }}
                                                    className="btn btn-danger">
                                                Удалить
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/*<div className="modal-footer">*/}
                            {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                            {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Partners);
