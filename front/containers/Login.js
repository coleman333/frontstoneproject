import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import SvgIcon from 'material-ui/SvgIcon';
import MainPage from './MainPage'
import authActions from '../actions/auth';
import {browserHistory} from 'react-router';
import Dialog from 'material-ui/Dialog';
import Formsy from 'formsy-react';
import FormsyText from 'formsy-material-ui/lib/FormsyText';
import search from '../../images/search.svg';

class Login extends Component {
    static propTypes = {
        // login:PropTypes.string,
        // password:PropTypes.string,
        // open: PropTypes.bool.isRequired

    };

    state = {
        canSubmit:false
    };

    constructor(props) {
        super(props);
    }


    handleClose = () => {
        if (this.props.onLoginToggle) {
            this.props.onLoginToggle(false)
        }
    };

    toggleButton(value) {
        this.setState({
            canSubmit: value
        })
    }

    submit() {
        let userData = this.refs.LoginFromForm.getCurrentValues();
        this.props.authActions.login(userData)
            .then((res)=> {
                this.handleClose();
                browserHistory.push('/');
                if (this.props.onSubmitSuccess) {
                    this.props.onSubmitSuccess();
                }
                //  this.props.open = false;
                // this.setState({open: true});
                this.setState({open:false});
            })

    }

    render() {
        const example = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                labelPosition="before"
                primary={true}
                disabled={!this.state.canSubmit}
                icon={<img src={search} alt="" style={{width: 20}}/>}
                onClick={this.submit.bind(this)}
            />
        ];

        return (

            <Dialog
                title="Login"
                modal={false}
                open={this.props.open}
                actions={example}
            >
                <Formsy.Form
                    // onValidSubmit={this.submit}
                    ref="LoginFromForm"
                    onValid={this.toggleButton.bind(this, true)}
                    onInvalid={this.toggleButton.bind(this, false)}
                >
                    {
                        // <div>
                        //     <FormsyText
                        //         validations={{isUrl:true, isLength:10}}
                        //         validationErrors={{isUrl:'not url', isLength:'should be 10 characters long'}}
                        //         name="text"
                        //         hintText="email"
                        //         // fullWidth={true}
                        //         floatingLabelText="email"
                        //         floatingLabelFixed={true}
                        //     />
                        // </div>
                    }
                    <div>
                        <FormsyText
                            name="email"
                            hintText="email"
                            type="email"
                            validations={{isEmail:true}}
                            validationErrors={{isEmail:'Email is not valid'}}
                            required={true}
                            // fullWidth={true}
                            floatingLabelText="email"
                            floatingLabelFixed={true}
                        />
                    </div>
                    <div>
                        <FormsyText
                            name="password"
                            hintText="password"
                            type="password"
                            required={true}
                            // fullWidth={true}
                            floatingLabelText="password"
                            floatingLabelFixed={true}
                        />
                    </div>
                </Formsy.Form>

            </Dialog>

        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {authActions: bindActionCreators(authActions, dispatch)}
};

export default connect(mapStateToProps, MapDispatchToProps)(Login);


