import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import supportIcon from '../../images/support.svg';
import officeAvatar from '../../images/officeAvatar.jpg';
import editIcon from '../../images/icon_edit.svg';
import editProfileIcon from '../../images/icon_profile_edit.svg';
import TextField from 'material-ui/TextField';
import addPhotoIcon from '../../images/icon_add_photo.svg';
import deleteUserIcon from '../../images/icon_delete.svg';
import passwdEditIcon from '../../images/icon_password_edit.svg';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';



import Avatar from 'material-ui/Avatar';

class Profile extends Component {
	static propTypes = {};

	state = {
	    // openEditWindow:false
        value: 3,
        // 7.2_Partners_User_Edit
    };

	constructor(props) {
		super(props);
        // this.state = {value: 3};
	}

    openEditProfileWindow(){
        $('#editUserModal').modal({ show: true });
    }

    openChangePasswdProfileWindow(){
        $('#changePasswordModal').modal({ show: true });
    }

    openDeleteUserProfileWindow(){
        $('#deleteUserModal').modal({ show: true });
        // $('#editUserModal').modal({ hide: true });
        $('#editUserModal').modal('hide')
    }


    // changeAvatar(){
    //     console.log(123434563456);
    // }

    handleChange = (event, index, value) => this.setState({value});

    uploadFile(){

    }

	render() {
		const {
			user = {
				firstName: 'Дмитрий',
				secondName: 'Заруба',
				role: 'superAdmin',
				phone: '+7 902 563 50 45',
				email: 'dzaruba@gmail.com'

			}
		} = this.props;

		return (
			<div style={{height: '100%', width: 'auto', backgroundColor: 'white', padding: 0}} className="row">
				<div className="col-lg-12" style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
					<div>
						<div className="row" >
							<div className="col-lg-12" style={{backgroundColor: '#f8f8f8',}}>
								<div style={{marginTop:'8%'}}>
									<span style={{marginLeft: '5%', color: '#5a5a5a',fontSize:'14px'}}>Простой Мир Управление</span>
								</div>
								<div>
									<span style={{marginLeft: '5%', color: '#6caaff', fontSize: '30px', fontWeight: 600}}>Профиль</span>
								</div>

							</div>
						</div>
						<div className="row">
							<div className='col-lg-12'>
								<div className="row" style={{marginLeft:'5%',marginTop:'5%'}}>
									<div className="" >
										<img className="imageClass" src={officeAvatar} alt="" style={{
											width: '120px',
											height: '120px',
											borderRadius: '50%',
											backgroundSize: 'auto 120px'
										}}/>
									</div>
									<div style={{marginLeft: '5%'}}>
										<div>
											<span style={{fontWeight: '3px', color: '#464e56'}}>C возвращением</span>
										</div>
										<div>
											<span style={{color: '#131517', fontWeight: '23'}}>{`${user.firstName} ${user.secondName}`}</span>
										</div>
										<div>
											<span style={{}}>{user.role}</span>
										</div>
										<div>
											<span style={{fontWeight: '3px',}}>{user.phone} | {user.email}</span>
										</div>
										<div className="row">
                                                <button style={{borderRadius:'20px',marginTop:'7%'}}
                                                        className="btn btn-success"
                                                        onTouchTap={this.openEditProfileWindow.bind(this)}>
                                                <img src={editIcon} style={{width:'25px'}} alt=""/>
                                                    Редактировать профиль
                                                </button>
                                                <button style={{borderRadius:'20px',marginTop:'7%',marginLeft:'10px',width:'250px'}}
                                                        className="btn btn-default"
                                                        onTouchTap={this.openChangePasswdProfileWindow.bind(this)}
                                                >
                                                    Изменить пароль
                                                </button>
                                        </div>

									</div>
								</div>
							</div>
						</div>

					</div>
					<div className='row' style={{marginBottom: 20}}>
						<div style={{display: 'flex', marginLeft: 100}}>
							<img src={supportIcon} alt="" style={{height: 50}}/>
							<div style={{
								display: 'flex',
								flexDirection: 'column',
								justifyContent: 'flex-end',
								marginLeft: 10
							}}>
								<div style={{fontSize: '12', color: '#a6b1c0'}}>Техническая поддержка</div>
								<div>
									<a href="#">Guardians@simpleword.com</a>
								</div>
							</div>
						</div>
					</div>
				</div>

                <div className="modal fade" id="editUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="editUserModalLabel" aria-hidden="true"
                     style={{width:'450px',marginLeft:'40%',marginTop:'8%'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{borderRadius:'20px'}}>
                            <div className="modal-header">
                                <img src={editProfileIcon} alt="" style={{width:'45px',marginTop:'10px'}}/>
                                <h3 className="modal-title" id="editUserModalLabel"
                                    style={{marginLeft:'5px',fontWeight:600}}>
                                    Редактировать <br/>данные</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <img className="imageClass" src={officeAvatar} alt="" style={{
                                        width: '70px',
                                        height: '70px',
                                        borderRadius: '50%',
                                        backgroundSize: 'auto 70px',
                                        marginLeft: '7%',
                                        marginTop: '2%'
                                    }}/>
                                    {/*<img src={addPhotoIcon} alt=""*/}
                                         {/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%',cursor:'pointer'}}*/}
                                          {/*onTouchTap={this.changeAvatar.bind(this)}/>*/}
                                    {/*<FloatingActionButton mini={true} secondary={true}*/}
                                                          {/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%'}}>*/}
                                        {/*/!*<img src={addPhotoIcon} style={{width:'25px'}} alt=""/>*!/*/}
                                        {/*<ContentAdd />*/}
                                        {/*<input type="file" onChange={this.uploadFile.bind(this)}/>*/}

                                    {/*</FloatingActionButton>*/}
                                    <label htmlFor="label123" style={{marginLeft:'-4%',width:'6%',marginTop:'10%',cursor:'pointer'}}>
                                        <img src={addPhotoIcon} style={{width:'35px'}} alt=""/>
                                        <input type="file" id="label123" style={{display:'none'}} onChange={this.uploadFile.bind(this)}/>
                                    </label>

                                    <img src={deleteUserIcon} alt="" style={{width:'20px',marginLeft:'15%',cursor:'pointer'}}/>
                                    <span style={{color: '#e6144a',cursor:'pointer'}}
                                          onTouchTap={this.openDeleteUserProfileWindow.bind(this)}>удалить пользователя</span>
                                </div>
                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="Имя Фамилия"
                                        fullWidth="true"
                                        ref="name"
                                    /><br/>

                                    <SelectField
                                        floatingLabelText="Роль"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        fullWidth="true"
                                    >
                                        <MenuItem value={1} primaryText="superadmin" />
                                        <MenuItem value={2} primaryText="admin" />
                                        <MenuItem value={3} primaryText="user" />

                                    </SelectField>

                                    <TextField
                                        hintText=""
                                        floatingLabelText="Телефон"
                                        fullWidth="true"
                                        ref="phone"
                                    /><br/>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="E-mail"
                                        fullWidth="true"
                                        ref="email"
                                    /><br/>
                                    <button type="button"
                                            style={{borderRadius:'20px',width:'30%',marginLeft:'70%',marginTop:'10%'
                                                }}
                                            className="btn btn-primary">
                                            Сохранить
                                    </button>
                                </div>
                            </div>
                            {/*<div className="modal-footer">*/}
                                {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                                    {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="changePasswordModal" tabIndex="-1" role="dialog"
                     aria-labelledby="changePasswordModalLabel" aria-hidden="true"
                     style={{width:'450px',marginLeft:'40%',marginTop:'8%'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{borderRadius:'20px'}}>
                            <div className="modal-header">
                                <img src={passwdEditIcon} alt="" style={{width:'45px',marginTop:'10px'}}/>
                                <h3 className="modal-title" id="changePasswordModalLabel"
                                    style={{marginLeft:'5px',fontWeight:600}}>
                                    Изменить<br/>пароль</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div>

                                </div>

                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <label htmlFor="">Пароль должен состоять из цыфр и букв,</label>
                                    <label htmlFor="">длинна пароля не менее 6 </label>

                                    <TextField
                                        hintText=""
                                        floatingLabelText="Введите старый пароль"
                                        fullWidth="true"
                                        type="password"
                                        ref="oldPassword"
                                    /><br/>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="Введите новый пароль"
                                        fullWidth="true"
                                        type="password"
                                        ref="newPassword"
                                    /><br/>
                                    <TextField
                                        hintText=""
                                        floatingLabelText="Введите пароль еще раз"
                                        fullWidth="true"
                                        type="password"
                                        ref="newPassword"
                                    /><br/><br/><br/><br/>
                                    <button type="button"
                                            style={{borderRadius:'20px',width:'35%',marginLeft:'70%',marginTop:'10%',
                                                marginBottom:'2%'}}
                                            className="btn btn-primary">
                                        Изменить
                                    </button>
                                </div>
                            </div>
                            {/*<div className="modal-footer">*/}
                            {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                            {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="deleteUserModal" tabIndex="-1" role="dialog"
                     aria-labelledby="deleteUserModalLabel" aria-hidden="true"
                     style={{width:'450px',marginLeft:'40%',marginTop:'8%'}}>
                    <div className="modal-dialog" role="document" >
                        <div className="modal-content" style={{borderRadius:'20px'}}>
                            <div className="modal-header">
                                <img src={ deleteUserIcon} alt="" style={{width:'75px',marginTop:'10px'}}/>
                                <h3 className="modal-title" id="deleteUserModalLabel"
                                    style={{marginLeft:'5px',fontWeight:600}}>
                                     Удаление<br/>пользователя</h3>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div style={{marginLeft: '7%', marginRight: '7%', marginTop: '1%'}}>
                                    <br/><br/>
                                    <label htmlFor="" style={{fontSize: '0.7vw'}}>Вы действительно хотите удалить пользователя?</label>
                                    <br/><br/><br/><br/>
                                    <div>
                                        <h3>{user.firstName} {user.secondName}</h3><br/>
                                        <span>{user.role}</span>
                                    </div>
                                    <br/><br/><br/><br/><br/><br/><br/><br/>
                                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'flex-end'}}>
                                            <div className="" style={{width:'35%'}}>
                                                <button type="button"
                                                        style={{borderRadius:'20px',width:'100%',
                                                            marginTop:'10%',marginBottom:'2%'}}
                                                        className="btn btn">
                                                    Отмена
                                                </button>
                                            </div >
                                            <div className="" style={{width:'35%'}}>
                                                <button type="button"
                                                        style={{borderRadius:'20px',marginLeft:'8%',marginTop:'10%'
                                                            , width:'100%',marginBottom:'2%'}}
                                                        className="btn btn-danger">
                                                    Удалить
                                                </button>
                                            </div>
                                    </div>
                                </div>

                            </div>
                            {/*<div className="modal-footer">*/}
                            {/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
                            {/*Сохранить</button>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Profile);
