import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";

class Partner extends Component {
	static propTypes = {};

	state = {};

	constructor(props) {
		super(props);
	}

	render() {
		const {children} = this.props;

		return (
			<div className={'row'} style={{ height:'100%'}}>
				<div className={'col-lg-2'} style={{ backgroundColor:'black', color:'white'}}>
					menu2
				</div>
				<div className={'col-lg-10'}>
					{children}
				</div>
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Partner);
