import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";

class Partners2 extends Component {
    static propTypes = {};

    state = {};

    constructor(props) {
        super(props);
    }

    render() {
        const {children} = this.props;

        return (
            <div className={'row'} style={{height: '100%',width:'100%'}}>
                <div className="row" >
                    <div className="col-lg-12" style={{backgroundColor: '#f8f8f8',}}>
                        <div style={{marginTop:'8%'}}>
                            <span style={{marginLeft: '5%', color: '#5a5a5a',fontSize:'14px'}}>Простой Мир Управление</span>
                        </div>
                        <div>
                            <span style={{marginLeft: '5%', color: '#6caaff', fontSize: '30px', fontWeight: 600}}>Профиль</span>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Partners2);
