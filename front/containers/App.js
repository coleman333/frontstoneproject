import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import "../assets/styles/main.scss";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

class App extends Component {
	static propTypes = {};

	state = {};

	constructor(props) {
		super(props);
	}

	render() {
		const muiTheme = getMuiTheme({});

		const {children} = this.props;

		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				{children}
			</MuiThemeProvider>
		)
	}

}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(App);
