import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
// import TextField from 'material-ui/TextField';
import Formsy from 'formsy-react';
import FormsyText from 'formsy-material-ui/lib/FormsyText';
import authActions from '../actions/auth';
import {browserHistory, Link} from 'react-router';
import _ from 'lodash';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

class Registration extends Component {
    static propTypes = {
        // open: PropTypes.bool.isRequired,
        onRegistrationToggle: PropTypes.func,
        onSubmitSuccess: PropTypes.func
    };

    state = {
        canSubmit: false
    };

    constructor(props) {
        super(props);
    }

    // componentWillMount(){
    //
    // }
    //
    // componentWillReceiveProps(nextProps){
    //
    // }
    //
    // shouldComponentUpdate(nextProps, nextState) {
    //     return this.props.open !== nextProps.open
    //     || !this.props.user.equals(nextProps.user)
    // }

    handleClose = () => {
        if (this.props.onRegistrationToggle) {
            this.props.onRegistrationToggle(false);
        }
        if (this.props.onLoginToggle) {
            this.props.onLoginToggle(false)
        }
    };

    submit(ev) {
        const userData = this.refs.regForm.getCurrentValues(); // {email:'',password:''}
        userData.files = this.state.selectedFile;
        // console.log(userData,this.state.selectedFile);
        // return;
        ev.persist()
        this.props.authActions.registration(userData)
            .then((res)=> {
                this.handleClose();
                browserHistory.push('/')
                if (this.props.onSubmitSuccess) {
                    this.props.onSubmitSuccess(ev);
                }
                // console.log('ok', res)
            })
            .catch(err=> {
                console.log(err)
            })
    }

    toggleButton(value) {
        this.setState({
            canSubmit: value
        })
    }

    uploadFile(ev) {
        // console.log(ev, ev.target.files)
        this.setState({selectedFile: _.first(ev.target.files)})
    }

    isEmail() {
        // console.log(44,email);
        const user = this.refs.regForm.getCurrentValues();
        const email = user.email;
        let popoverIsEmail;
        this.props.authActions.ISEmail(email)
            .then((res)=> {
                console.log(11, res)
                if (res.response.data.isEmailResult) {
                    // alert('your email is already exists')

                    popoverIsEmail = true;
                    this.handleTouchTap(popoverIsEmail);
                }
                else {
                    popoverIsEmail = false;
                    this.handleTouchTap(popoverIsEmail);

                }
            })
            .catch(err=> {
                console.log(err)
            })
    }
    onClickPopover(){
        this.handleRequestClose()
    }


    handleTouchTap = (popoverIsEmail) => {

        if (!popoverIsEmail) {
            this.setState({
                open: true,
                anchorEl: this.refs.email.muiComponent.input,
                popoverIsEmail: 'correct email'
            });
        }
        else {
            this.setState({
                open: true,
                anchorEl: this.refs.email.muiComponent.input,
                popoverIsEmail: "your email is already exists"
            });
        }
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };

    render() {

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                disabled={!this.state.canSubmit}
                onTouchTap={this.submit.bind(this)}
            />,
            <FlatButton
                label="Choose an Image"
                labelPosition="before"
                containerElement="label"
            >
                <input type="file" onChange={this.uploadFile.bind(this)}/>
            </FlatButton>

        ];
        return (
            <div>

                <Dialog
                    title="Registration"
                    actions={actions}
                    modal={false}
                    open={this.props.open}
                    // onRequestClose={this.handleClose}
                >
                    <Formsy.Form
                        // onValidSubmit={this.submit}
                        ref="regForm"
                        onValid={this.toggleButton.bind(this, true)}
                        onInvalid={this.toggleButton.bind(this, false)}
                    >
                        {
                            // <div>
                            //     <FormsyText
                            //         validations={{isUrl:true, isLength:10}}
                            //         validationErrors={{isUrl:'not url', isLength:'should be 10 characters long'}}
                            //         name="text"
                            //         hintText="email"
                            //         // fullWidth={true}
                            //         floatingLabelText="email"
                            //         floatingLabelFixed={true}
                            //     />
                            // </div>
                        }
                        <div>
                            <FormsyText
                                name="firstName"
                                ref="firstName"
                                hintText="enter firstName"
                                // type="text"
                                floatingLabelText="firstName"
                                // floatingLabelFixed={true}
                            />
                        </div>
                        <div>
                            <FormsyText
                                name="lastName"
                                ref="lastName"
                                hintText="enter lastName"
                                // type="text"
                                floatingLabelText="lastName"
                                // floatingLabelFixed={true}
                            />
                        </div>
											<div>
												<FormsyText
													name="organisation"
													ref="organisation"
													hintText="enter organisation"
                          // type="text"
													floatingLabelText="organisation"
													// floatingLabelFixed={true}
												/>
											</div>
                        <div>
                            <FormsyText
                                name="email"
                                hintText="email"
                                type="email"
                                ref="email"
                                validations={{isEmail:true}}
                                validationErrors={{isEmail:'Email is not valid'}}
                                required={true}
                                // fullWidth={true}
                                floatingLabelText="email"
                                // floatingLabelFixed={true}
                                onBlur={this.isEmail.bind(this)}
                            />
                        </div>
                        <div>
                            <FormsyText
                                name="password"
                                hintText="password"
                                type="password"
                                required={true}
                                // fullWidth={true}
                                floatingLabelText="password"
                                // floatingLabelFixed={true}
                            />
                        </div>
                    </Formsy.Form>
                </Dialog>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.handleRequestClose}
                    // popoverIsEmail={this.state.popoverIsEmail}
                >
                    <Menu>
                        <MenuItem primaryText={this.state.popoverIsEmail} onClick={this.onClickPopover.bind(this)}/>

                    </Menu>

                </Popover>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    }
};

export default connect(mapStateToProps, MapDispatchToProps)(Registration);


