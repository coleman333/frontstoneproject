import {createReducer} from 'redux-act';
import {fromJS} from 'immutable';
import utils from '../utils';

import authActions from '../actions/auth';


const initialState = fromJS({
    processing: 0,
    error: {},
    user: {},
    value: ''
    // cards: []
});

export default createReducer({
    [authActions.registration.request]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') + 1)
    },
    [authActions.registration.ok]: (state, payload) => {
        state = utils.setProcessingToState(state, state.get('processing') - 1)
        return state
            .update('user', user=>fromJS(payload.response.data))
    },
    [authActions.registration.error]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') - 1)
    },
    
    [authActions.login.request]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') + 1)
    },
    [authActions.login.ok]: (state, payload) => {
        state = utils.setProcessingToState(state, state.get('processing') - 1)
        return state
            .update('user', (user)=>fromJS(payload.response.data))
    },
    [authActions.login.error]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') - 1)
    },

    [authActions.logout.request]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') + 1)
    },
    [authActions.logout.ok]: (state, payload) => {
        state = utils.setProcessingToState(state, state.get('processing') - 1)
        return state
            .update('user', user=>user.clear())
    },
    [authActions.logout.error]: (state, payload) => {
        return utils.setProcessingToState(state, state.get('processing') - 1)
    }

    
}, initialState);
