import {combineReducers} from 'redux-immutable';
import routerReducer from './routerReducer';
import auth from './auth';
import cardReducer from './cardReducer';

export default combineReducers({
    routing: routerReducer,
    auth: auth,
    cardReducer:cardReducer
});
