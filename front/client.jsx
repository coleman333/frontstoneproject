import React from 'react';
import Immutable from 'immutable';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, browserHistory} from 'react-router';
import createRoutes from './routes.jsx';
import configureStore from './store/configureStore';
import {syncHistoryWithStore} from 'react-router-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// Grab the state from a global injected into
// server-generated HTML
const initialState = Immutable.fromJS(window.__INITIAL_STATE__);

// Allow the passed state to be garbage-collected
delete window.__INITIAL_STATE__;

const store = configureStore(initialState);

const history = syncHistoryWithStore(browserHistory, store, {
    selectLocationState (state) {
        return state.get('routing').toObject();
    }
});

const routes = createRoutes(store);
// Router converts <Route> element hierarchy to a route config:
render(
    <Provider store={store}>
        <Router history={history}>
            {routes}
        </Router>
    </Provider>, document.getElementById('app')
);
