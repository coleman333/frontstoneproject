import {createStore, applyMiddleware} from 'redux';
import Immutable from 'immutable';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import {createLogger}from 'redux-logger';

function configureStore(initialState = Immutable.Map()) {

    let middleware = [thunk];
    try {
        if (__DEV__ && !__SERVER__) {
            middleware.push(createLogger({
                stateTransformer: (state) => state.toJS()
            }));
        }
    } catch (ex) {
        // console.error(ex);
    }
    // Installs hooks that always keep react-router and redux
    // store in sync
    const finalCreateStore = applyMiddleware(...middleware)(createStore);

    const store = finalCreateStore(rootReducer, initialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('reducers', () => {
            const nextReducer = rootReducer;
            store.replaceReducer(nextReducer);
        });
    }
    return store;
}

export default configureStore;