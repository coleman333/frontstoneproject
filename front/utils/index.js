function setProcessingToState(state, value) {
    if ( __SERVER__ ) {
        return state;
    }
    return state
        .set('processing', value)
}

module.exports={
    setProcessingToState
};
