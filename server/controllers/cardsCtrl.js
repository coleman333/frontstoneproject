var passport = require('passport');
var path = require('path');
var multer = require('multer');
let userModel = require('../models/userModel');
let cardModel = require('../models/cardModel');
const _ = require('lodash');

module.exports.upload = multer({
	storage: multer.diskStorage({
		destination: function (req, file, cb) {
			var absolutePath = path.join(__dirname, '../../images/');
			cb(null, absolutePath);
		},
		filename: function (req, file, cb) {
			cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
		}
	})
});


module.exports.loadAllPosts = function (req, res, next) {
	const {userId, cardLimit, skipCards} = req.body;
	console.log(1234145345, userId, cardLimit, skipCards);

	// cardModel.find({idUser: userId},null,{ skip: skipCards, limit: cardLimit }, function (err, cards) {
	// 	if (err) {
	// 		console.error(err);
	// 		return next(err);
	// 	}
	// 	console.log(cards);
	// 	return res.json({	 cards });
	//
	// })
	cardModel.find({idUser: userId}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards).exec(function (err, cards) {
		if (err) {
			console.error(err);
			return next(err);
		}
		cardModel.count({idUser: userId}, function (err, countCards) {
			if (err) {
				console.log('mistake in count cards', countCards);
			}
			// console.log(idUser)
			console.log(countCards);
			return res.json({cards, countCards});
		});

	})
};

module.exports.loadCardsMainPage = function (req, res, next) {
	const {cardLimit, skipCards} = req.body;
	const cards = {};

	const HZCards = new Promise((resolve, reject) => {
		cardModel.find({category: 'hydroZone'}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards)
			.exec(function (err, hydroZoneCards) {
				if (err) {
					console.log('mistake in hydro cards', hydroZoneCards);
					console.error(err);
					return reject(err);
				}
				cards.hydroZoneCards = hydroZoneCards;
				resolve();
			})
	});
	const countHZCards = new Promise((resolve, reject) => {
		cardModel.count({category: 'hydroZone'}, function (err, countHydroZoneCards) {
			if (err) {
				console.log('mistake in count hydro cards', countHydroZoneCards);
				console.error(err);
				return reject(err);
			}
			cards.countHydroZoneCards = countHydroZoneCards;
			resolve();
		})
	});



	Promise.all([HZCards, countHZCards, EZCards, countEZCards, SZCards, countSZCards]).then(value => {
		// console.log(value);
		// console.log(cards);
		return res.json({cards});
	}, err => {
		console.log(err)
		next(err)
	});

	// return new Promise(function (resolve, reject) {
	// 	cardModel.find({category: 'hydroZone'}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards)
	// 		.exec(function (err, hydroZoneCards) {
	// 			if (err) {
	// 				console.error(err);
	// 				return reject(err);
	// 			}
	// 			cards.hydroZoneCards = hydroZoneCards;
	// 			resolve();
	// 		})
	// })
	// 	.then(() => {
	// 		cardModel.count({category: 'hydroZone'}, function (err, countHydroZoneCards) {
	// 			if (err) {
	// 				console.log('mistake in count cards', countHydroZoneCards);
	// 			}
	// 			return res.json({hydroZoneCards, countHydroZoneCards});
	// 		})
	// 	});

};

module.exports.redirectToUserCard = function (req, res, next) {
	let {idUser, idCard} = req.body;
	// idCard = mongoose.Types.ObjectId.fromString(idCard);
	new Promise((resolve, reject) => {
		cardModel.findById(idCard)
			.exec(function (err, card) {
				if (err) {
					// console.error(err);
					return reject(err);
				}

				if (!card) {
					return reject(new Error('There is no card found.'));
				}
				// console.log('before culk',countCard);
				// countCard= countCard %10?(Math.round(countCard/10)+1)*10:countCard;
				// console.log('after culk',countCard);
				resolve(card);
			})
	})
		.then((card) => {
			return new Promise((resolve, reject) => {
				cardModel.find({idUser: idUser, datePosted: {$gte: card.datePosted}}).sort({datePosted: -1})
					.exec(function (err, cards) {
						if (err) {
							// console.error(err);
							return reject(err);
						}
						resolve(cards);
					})
			})
		})
		.then((cards) => {
			// console.log(6543, cards);
			console.log('this is id', idCard);
			console.log(`\n\n=>>`, cards)
			return res.json({cards});
		})
		.catch(err => {
			console.log(err);
			next(err)
		})
}
