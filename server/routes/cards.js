var express = require('express');
var router = express.Router();
const cardCtrl = require('../controllers/cardsCtrl');

router.post('/loadCards', cardCtrl.loadAllPosts);
router.post('/loadCardsMP', cardCtrl.loadCardsMainPage);
router.post('/redirectToUserCard',cardCtrl.redirectToUserCard);


module.exports = router;



