// App is a function that requires store data and url to initialize and return the React-rendered html string - server-side rendering
const App = require('../../public/assets/front.server.js');
const auth = require('./users');
const card = require('./cards');

module.exports = (app) => {

    app.use('/api/users', auth);
    app.use('/api/cards',card);
    
    app.get('*', (req, res, next) => {
        App(req, res);
    });

// error handler
// development error handler
// will print stacktrace
    if (app.get('env') === 'development') {
        app.use((err, req, res, next) => {
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
        });
    } else {
// production error handler
// no stacktraces leaked to user
        app.use((err, req, res, next) => {
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: {}
            });
        });
    }
};
