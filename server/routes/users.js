var express = require('express');
var router = express.Router();
const userCtrl = require('../controllers/userCtrl');


router.get('/', userCtrl.fetch);
router.post('/registration', userCtrl.upload.single('avatar'), userCtrl.registration);
router.post('/login', userCtrl.login);
router.get('/logout', userCtrl.logout);
router.post('/isEmail', userCtrl.isEmail);
// router.post('/isUser',userCtrl.isUser);
//     (req, res, next)=> {
//     console.log(req.body)
//     res.json(req.body)
// })

module.exports = router;


