module.exports = {
    sessionSecret: process.env.SESSION_SECRET || 'sessionSecret',
    database: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || 27017,
        dbname: process.env.DB_NAME || 'asdf',
    }
};
