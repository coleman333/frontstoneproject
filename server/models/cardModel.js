const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const cardSchema = new Schema({
    avatar: {type: String, required: false},
	  organisation: {type: String, required: false},
    signUnderPicture: {type: String, required: false},
    textUnderPicture: {type: String, required: false},
    CardTitle: {type: String, required: false},
    CardSubtitle: {type: String, required: false},
    text: {type: String, required: false},
    idUser: {type: String, required: true},
		firstName: {type: String, required: false},
    file:{type:String, required:false},
	  fileType:{type:String, required:false},
    datePosted:{type:Date,required:true},
		category:{type:String,required:false}
});
//
// userSchema.pre('save', function (next) {
//     const user = this;
//
//     // only hash the password if it has been modified (or is new)
//     if (!user.isModified('password')) return next();
//
//     // generate a salt
//     bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
//         if (err) return next(err);
//
//         // hash the password along with our new salt
//         bcrypt.hash(user.password, salt, (err, hash) => {
//             if (err) return next(err);
//
//             // override the cleartext password with the hashed one
//             user.password = hash;
//             next();
//         });
//     });
// });

module.exports = mongoose.model('Card', cardSchema);
